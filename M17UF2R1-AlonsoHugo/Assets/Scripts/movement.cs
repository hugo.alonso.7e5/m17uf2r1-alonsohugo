using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    public float Speed;
    Rigidbody2D m_rb;
    private Animator _anim;
    private SpriteRenderer spRd;

    private void Start()
    {
        spRd = GetComponent<SpriteRenderer>();
        _anim = GetComponent<Animator>();
        Speed = 200f;
        m_rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        float movimientoH = Input.GetAxisRaw("Horizontal");
        float movimientoV = Input.GetAxisRaw("Vertical");

        Vector3 m_Input = new Vector3(movimientoH*Time.deltaTime, movimientoV* Time.deltaTime);

        _anim.SetBool("Movement", movimientoH != 0||movimientoV!=0);

        if (movimientoH < 0)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
            
        }
        if (movimientoH > 0)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }
        m_rb.MovePosition(transform.position+m_Input*Time.deltaTime*Speed);
    }
}
