using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemy1;
    public GameObject enemy2;
    public Vector2 movement;
    public int enemyAmount = 10;

    private void Start()
    {
        InvokeRepeating("enemy", 1.5f, 1.5f);
    }
    private void Update()
    {
        
    }
    private void enemy()
    {
        movement = new Vector2(Random.Range(25f, 48f), Random.Range(2f, 24.6f));
        var randomNum = Random.Range(0, 2);
        if (randomNum == 1)
        {
            Instantiate(enemy1, movement, Quaternion.identity);

        }
        else if (randomNum == 0)
        {
            Instantiate(enemy2, movement, Quaternion.identity);
        }
    }
}
