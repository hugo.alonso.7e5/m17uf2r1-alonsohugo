using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerInfo : MonoBehaviour
{
    [SerializeField] private PlayerSO pInfo;
    private int _hp;
    public Image healthBar;
    public float healthAmount;
    [SerializeField]public Inventario inventario;
    int index;
    public GameObject arma;

    void Start()
    {
        _hp = pInfo.Health;
        healthAmount = (float)_hp;
        index = 0;
        ChangeWeapon();
    }

    void Update()
    {
        if (healthAmount<=0)
        {
            SceneManager.LoadScene("LoseScene");
        }

        if (Input.GetKey(KeyCode.C))
        {
            index++;
            ChangeWeapon();
        }
    }

    public void TakeDamage(float damage)
    {
        healthAmount -= damage;
        healthBar.fillAmount = healthAmount / 100f;
    }

    private void ChangeWeapon()
    {
        arma = GameObject.Find("Arma");
        arma = inventario.armas[index];
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy1"))
        {
            TakeDamage(0.2f);
        }
        
    }
}
