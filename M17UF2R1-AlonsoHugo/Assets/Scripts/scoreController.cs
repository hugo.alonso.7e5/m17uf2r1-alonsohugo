using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreController : MonoBehaviour
{
    public static GameObject punt;
    public static double punts = 0;

    private void Start()
    {
        punt = GameObject.Find("Puntuacio");
    }

    private void Update()
    {
        punt.GetComponent<Text>().text = punts.ToString();
    }
    public static void puntos()
    {
        punts += 0.5;
        punt.GetComponent<Text>().text = punts.ToString();
    }

}
