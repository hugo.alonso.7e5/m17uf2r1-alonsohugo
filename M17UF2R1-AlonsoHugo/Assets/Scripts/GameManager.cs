using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public GameObject jugador;
    public Camera CamaraJuego;
    public static GameObject punt;
    public static double punts = 0;
    public static double counter=20;
    // Start is called before the first frame update
    void Start()
    {
        punts = 0;
        counter = 20;
        punt = GameObject.Find("Puntuacio");
    }

    // Update is called once per frame
    void Update()
    {
        punt.GetComponent<Text>().text = punts.ToString();
        if (jugador != null)
        {
            CamaraJuego.transform.position = new Vector3(
                jugador.transform.position.x,
                jugador.transform.position.y,
                CamaraJuego.transform.position.z
                );
        }
    }

    public static void puntos()
    {
        punts += 5;
        punt.GetComponent<Text>().text = punts.ToString();
    }

    public static void enemyCounter()
    {
        counter -= 0.5;
        Debug.Log(counter);
        if (counter <= 0)
        {
            SceneManager.LoadScene("WinScene");
        }
    }

}
