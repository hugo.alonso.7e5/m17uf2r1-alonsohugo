
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventario",menuName = "Inventario")]

public class Inventario : ScriptableObject
{
    public GameObject[] armas = new GameObject[5];
    public void VaciarInventario()
    {
        armas = new GameObject[5];
    }

}
