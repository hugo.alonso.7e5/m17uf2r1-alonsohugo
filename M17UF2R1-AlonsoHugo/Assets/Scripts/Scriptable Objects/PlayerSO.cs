using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Data", menuName = "Player Data")]

public class PlayerSO : ScriptableObject
{
    [SerializeField] private string _name;
    
    [SerializeField] private int _health;

    public string Name { get { return _name; } }
    public int Health {get { return _health; } }
}
