using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shot : MonoBehaviour
{
    public GameObject bullet;
    public Transform firePoint;
    private float bulletSpeed = 10f;
    Vector3 lookPos;
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        lookPos = Camera.main.ScreenToWorldPoint(mousePos) - transform.position;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Shoot();
        }

    }
    private void Shoot()
    {

        GameObject tempBullet =Instantiate(bullet,firePoint.position, firePoint.rotation);
        Rigidbody2D rb = tempBullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.right * bulletSpeed, ForceMode2D.Impulse);
    }
}
